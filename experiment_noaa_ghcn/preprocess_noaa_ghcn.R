set.seed(42)

## Read inventory data
inv <- read.table(file="./experiment_noaa_ghcn/ghcnd-inventory.txt",
                  col.names=c("id","lat","lon","element","firstyear","lastyear"),
                  header=F)
inv$yeardiff <- inv$lastyear - inv$firstyear

inv.s <- subset(inv, element=="TMAX")
inv.s[which.max(inv.s$yeardiff),]


## Read daily data

# ------------------------------
#   Variable   Columns   Type
# ------------------------------
# ID            1-11   Character
# YEAR         12-15   Integer
# MONTH        16-17   Integer
# ELEMENT      18-21   Character
# VALUE1       22-26   Integer
# MFLAG1       27-27   Character
# QFLAG1       28-28   Character
# SFLAG1       29-29   Character
# VALUE2       30-34   Integer
# MFLAG2       35-35   Character
# QFLAG2       36-36   Character
# SFLAG2       37-37   Character
# .           .          .
# .           .          .
# .           .          .
# VALUE31    262-266   Integer
# MFLAG31    267-267   Character
# QFLAG31    268-268   Character
# SFLAG31    269-269   Character
# ------------------------------
widths <- c(11,4,2,4)
val.block.widths <- c(5,1,1,1)
widths <- c(widths, rep(val.block.widths,31))

col.names <- c("id","y","m","element")
val.block.names <- c("value","mflag","qflag","sflag")
tmp.names <- NULL
for (i in 1:31){
  tmp.names <- c(tmp.names, paste(val.block.names, sprintf("_%d",i), sep=""))
}
col.names <- c(col.names, tmp.names)

milan <- read.fwf(file="./experiment_noaa_ghcn/ITE00100554.dly.txt", widths=widths,
                  header=F, col.names=col.names, stringsAsFactors=F)


## Extract one feature for analysis
milan.tmax <- subset(milan, element=="TMAX")[,c("y","m","element",grep("value_.*", names(milan), value=T))]
#order(milan.tmax$y, milan.tmax$m)

# Convert into matrix [months, days]
milan.tmax.mat <- as.matrix(milan.tmax[,4:dim(milan.tmax)[2]])
milan.tmax.mat[milan.tmax.mat==-9999] <- NA
milan.tmax.mat <- milan.tmax.mat/10 #given in tenths of degree
#milan.tmax.month <- apply(milan.tmax.mat,1,mean,na.rm=T)

# Collect monthly averages
milan.tmax.m <- milan.tmax[,1:2]
milan.tmax.m$tmax <- apply(milan.tmax.mat,1,mean,na.rm=T) #monthly averages
milan.tmax.m <- as.data.frame(cast(milan.tmax.m, y~m, value="tmax"))

# Drop missing data
milan.tmax.m.mat <- as.matrix(milan.tmax.m[,2:13])
dimnames(milan.tmax.m.mat) <- list(milan.tmax.m$y, 1:12)
data <- milan.tmax.m.mat[rowSums(is.na(milan.tmax.m.mat))==0,]


## Save processed data for fast loading
dimnames(data) <- NULL
data.list <- list(time = 1:12, sig=data)
saveRDS(data.list, "./experiment_noaa_ghcn/ITE00100554_tmax_monthly.rds")


## Find out lowest achievable confidence level
L = 245
fold <- mwe.fold.profile(data, L, interrupt.at=243)
saveRDS(fold, "./experiment_noaa_ghcn/ITE00100554_tmax_FWER_profile_MWE.rds")
