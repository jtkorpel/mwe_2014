# MWE algorithm
# This implementation is an R implementation of the one given in the manuscript.

create.Rj <- function(dvec){
  N <- length(dvec)
  Rj <- list()
  Rj$dll <- list(prev=c(NA, 1:(N+1)), nxt=c(2:(N+2),NA), obs.ind=c(NA,order(dvec),NA))
  Rj$lookup <- order(order(dvec))+1
  Rj
}


get.smallest <- function(Rj, pos){
  # Input:
  #   Rj  list, Rj data structure
  #   pos int, 1==smallest, 2==2nd smallest, etc.
  ind <- 1
  for (k in 1:pos){
    ind <- Rj$dll$nxt[ind]
  }
  Rj$dll$obs.ind[ind]
}


get.largest <- function(Rj, pos){
  # Input:
  #   Rj  list, Rj data structure
  #   pos int, 1==largest, 2==2nd largest, etc.
  ind <- length(Rj$dll$prev)
  for (k in 1:pos){
    ind <- Rj$dll$prev[ind]
  }
  Rj$dll$obs.ind[ind]
}


remove.observation.Rj <- function(Rj, obs.ind){
  Rj$dll$prev[Rj$dll$nxt[Rj$lookup[obs.ind]]] <- Rj$dll$prev[Rj$lookup[obs.ind]]
  Rj$dll$nxt[Rj$dll$prev[Rj$lookup[obs.ind]]] <- Rj$dll$nxt[Rj$lookup[obs.ind]]

  Rj
}


list.elements <- function(dvec, Rj){
  ind <- 1
  end.reached <- F
  cat(sprintf("head"))
  while (!end.reached){
    ind <- Rj$dll$nxt[ind]
    if (is.na(Rj$dll$obs.ind[ind])){
      end.reached=T
      cat(sprintf(", tail.\n"))
    }
    else {
      cat(sprintf(", %1.2f", dvec[Rj$dll$obs.ind[ind]]))
    }
  }
}


create.R <- function(dmat){
  R <- list()
  for (j in 1:ncol(dmat)){
    R <- c(R, list(create.Rj(dmat[,j])))
  }
  R
}


## Minimum width envelope confidence region
#
# Input:
#   dmat   (N,M) numeric matrix, N samples, M dimensions/variables. The samples define the 
#             empirical cdfs of the m variables.
#   k         [1,1] integer, The desired number of observations to remove, usually k=ceiling(alpha*N)
#
# Output:
#   A list with elements:
#   extreme.obs.inds  [1,ceiling(alpha*N)] integer, Indices of "most extreme" rows i.e.
#                     rows that do not belong to the 1-alpha envelope
#   is.extreme.obs    [1,N] locigal, same information in logical vector
mwe.ci.R <- function(dmat, k){
  require(hash)
  N = nrow(dmat)
  M = ncol(dmat)
  
  # Initialize data structures:
  R <- create.R(dmat)
  extreme.obs.inds <- NULL
  central.obs.inds <- 1:N # Keep track of which rows have not yet been removed
  
  n.extr.rows <- 0
  while (n.extr.rows < k){
    
    # Find the optimal row to remove (maximize confidence region shrinkage)
    dU <- hash()
    
    for (m in 1:M){
      # Add gains from extreme points to the corresponding observations
      max1i <- get.largest(R[[m]], 1)
      max2i <- get.largest(R[[m]], 2)
      if (has.key(make.keys(max1i),dU)){
        # append to existing
        dU[make.keys(max1i)] <- dU[[make.keys(max1i)]] + abs(dmat[max1i,m]-dmat[max2i,m])
      } else {
        # make new hash table entry
        dU[make.keys(max1i)] <- abs(dmat[max1i,m]-dmat[max2i,m])
      }
      
      min1i <- get.smallest(R[[m]], 1)
      min2i <- get.smallest(R[[m]], 2)    
      if (has.key(make.keys(min1i),dU)){
        # append to existing
        dU[make.keys(min1i)] <- dU[[make.keys(min1i)]] + abs(dmat[min2i,m]-dmat[min1i,m])
      } else {
        # make new hash table entry
        dU[make.keys(min1i)] <- abs(dmat[min2i,m]-dmat[min1i,m])
      }
    } # O(M)
    
    n.opt <- as.numeric(names(which.max(values(dU))))
    extreme.obs.inds <- c(extreme.obs.inds, n.opt)
    n.extr.rows <- n.extr.rows + 1
    
    # Update data structure R
    for (m in 1:M){
      R[[m]] <- remove.observation.Rj(R[[m]], n.opt)
    }
  } #of while
  
  return(list(extreme.obs.inds=extreme.obs.inds,
              is.extreme.obs=1:N %in% extreme.obs.inds,
              k=k,
              method="MWER"))
}


# A wrapper to call mwe.ci.R using alpha rather than k
mwe.ci.R.a <- function(data.rs, alpha, L=NULL){
  ci.mwe <- mwe.ci.R(data.rs, floor(alpha*nrow(data.rs))) 
  ci.mwe$target.alpha <- alpha
  return(ci.mwe)
}
