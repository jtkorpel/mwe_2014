#!/bin/bash

RECORDS=(106 119 200)

for REC in ${RECORDS[@]}
do
    echo $REC
    rdsamp -r mitdb/$REC -c -H -f 0 -v -pd > $REC.csv
done
