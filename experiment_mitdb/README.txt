Data source:
http://www.physionet.org/cgi-bin/atm/ATM on 7.1.2014

Loaded using anhe's machine and the associated bash script.

Sampling rate: 360 Hz
Range: over 10 mV
Resolution: 11 bit

Database: MIT-BIH Arrhythmia Databasa (mitb)
Records: 106
Signals: all
Annotations: reference beat, rhythm, and signal quality annotations
Length: to end
Time format: seconds
Data format: standard
