# A script to visualize confidence band width

set.seed(42)
require(scales)

########################################################################################
## Setup
savepath.fig = file.path(OPTS$res.dir, 'pubfigs')


########################################################################################
## Helper functions
agg.fun <- function(df, variable){
  ret.df <- data.frame( dlen=length(df[,variable]),
                        mean=mean(df[,variable]),
                        se=stderr(df[,variable]))
  return(ret.df)
}


########################################################################################
## Load data

# Old style:
# loadpath <- file.path(OPTS$work.dir,'data_intermediate/batch.synthdata.runtime')
# data.set.id <- 'BKP_MVNormHB_mvnorm.heartbeat.1_nonsc'
# data.file.name <- sprintf('%s_alpha%1.2f_ConfBandsNData.rds',data.set.id,alpha)
# rl <- readRDS(file.path(loadpath, data.file.name))

nm.arr <- matrix(c(100, 25,
                   500, 25,          
                   1e4, 25,
                   5e4, 25,
                   1e5, 25,
                   1e4, 10,
                   1e4, 50,
                   1e4, 100), ncol=2, byrow=T)
# 1e4, 500,
# 1e4, 1e3
#1e3, 25,
loadpath <- file.path(OPTS$work.dir,'data_intermediate/batch.synthdata.runtime')
rl <- load.cbANDdata(loadpath, nm.arr,
                      "MVNormHB", "mvnorm.heartbeat.1_nonsc_alpha0.10_ConfBandsNData.rds")

## Aggregate results 
pd <- ddply(rl$key,  .(N,M,method), agg.fun, variable="run.time")
pd$NlgN <- pd$N*log(pd$N)

##############################################################################################
## Visualize results
methods.to.include <- c("QUANTILE","BONFERRONI","DST.MAHA","DST.L2","MWE")
#methods.to.include <- "MWE"

pd <- subset(pd, method %in% methods.to.include)

inds <- unlist(lapply(methods.to.include,
                      function(x){which(OPTS$method.name.lookup$working.name %in% x)}))
pd$method <- ordered(pd$method, methods.to.include, OPTS$method.name.lookup$publication.name[inds])
cdef <- OPTS$plot$cdef[match(methods.to.include, OPTS$plot$cdef$method),]

# cdef <- subset(OPTS$plot$cdef, method %in% methods.to.include)
# pd$method <- ordered(pd$method, methods.to.include)
# cdef <- cdef[match(methods.to.include, cdef$method),]

## Run time by N
m <- 25
pds <- subset(pd, M==m)
p <- ggplot(data=pds)
p <- p + geom_line(aes(y=mean/60, x=NlgN, color=method, linetype=method))
p <- p + geom_pointrange(aes(y=mean/60, x=NlgN, ymin=(mean-se)/60, ymax=(mean+se)/60, color=method, width=30), show_guide=FALSE)
p <- p + scale_linetype_manual(values=cdef$linetype)
p <- p + scale_colour_manual(values=cdef$color)
p <- p + scale_x_continuous(labels=function(x){scientific(x, digits=1)})
p <- p + theme_bw()
p <- p + labs(x="N*log(N)", y="running time (min)", title=sprintf("M=%d",m))
#print(p)

## Run time by M
n <- 1e4
pds <- subset(pd, N==n)
p2 <- ggplot(data=pds)
p2 <- p2 + geom_line(aes(y=mean/60, x=M, color=method, linetype=method))
p2 <- p2 + geom_pointrange(aes(x=M, y=mean/60, ymin=(mean-se)/60, ymax=(mean+se)/60, color=method, width=30), show_guide=FALSE)
p2 <- p2 + scale_linetype_manual(values=cdef$linetype)
p2 <- p2 + scale_colour_manual(values=cdef$color)
p2 <- p2 + theme_bw()
p2 <- p2 + labs(x="M", y="running time (min)", title="N=10^4")
#print(p2)


savename <- "running_times_N.eps"
ggsave(filename=file.path(savepath.fig,savename), plot=p, 
       width = OPTS$plot$aspect.ratio*OPTS$plot$fig.height.cm,
       height = OPTS$plot$fig.height.cm, units="cm", scale=0.5)
savename <- "running_times_M.eps"
ggsave(filename=file.path(savepath.fig,savename), plot=p2, 
       width = OPTS$plot$aspect.ratio*OPTS$plot$fig.height.cm,
       height = OPTS$plot$fig.height.cm, units="cm", scale=0.5)

w.inch = 2*4.5
h.inch = 2*1.5
savename <- "running_times.eps"
postscript(file.path(savepath.fig, savename), width=w.inch, height=h.inch)
grid.arrange(p, p2, ncol=2)
dev.off()
savename <- "running_times.pdf"
pdf(file.path(savepath.fig, savename), width=w.inch, height=h.inch)
grid.arrange(p, p2, ncol=2)
dev.off()

