# A script to create the toyExample

#source("./publication_plots//pubplots_tools.R")
set.seed(42)

## Options
savepath <- file.path(OPTS$res.dir, 'pubfigs/toyExample')
dir.create(savepath, recursive=T, showWarnings=F)

## Load data
d <- load.heartbeat.normal()
N <- nrow(d$sig)
M <- ncol(d$sig)

## Compute confidence bands
ci.mwe <- mwe.ci(d$sig, floor(alpha*N))
env.mwe <- ci.envelope(d$sig[!ci.mwe$is.extreme.obs,])

ci.qnt <- ci.quantile(d$sig, alpha)
env.qnt <- ci.envelope(d$sig[!ci.qnt$is.extreme.obs,])

L = 5
n.interrupt <- ceiling(((L-1)/L)*N*alpha)
fold <- mwe.fold.profile(d$sig, L, interrupt.at=n.interrupt)
fold$profile[1:20]
plot(fold$profile)
(k.eff <- max(which(fold$profile <= alpha))) #8
cat(sprintf("k_eff: %d\n",k.eff))

ci.mwe.eff <- mwe.ci(d$sig, k.eff)
env.mwe.eff <- ci.envelope(d$sig[!ci.mwe.eff$is.extreme.obs,])


## Plot and save
fig.height.inch = 6

# Note: postscript() does not support semi-transparency -> cannot do eps directly

savename <- "intro_toyExample.pdf"
pdf(file = file.path(savepath, savename),
    width = OPTS$plot$aspect.ratio*fig.height.inch , height = fig.height.inch)
matplot(d$time, t(d$sig), type="l", lty=1, col=rgb(0,0,0,0.01),
        xlab="Time (s)", ylab="Voltage (mV)")
matlines(d$time, t(env.qnt$envelope), type="l", lty=1, lwd=1, col=rgb(1,0,0,1))
matlines(d$time, t(env.mwe$envelope), type="l", lty=1, lwd=1, col=rgb(0,0,1,1))
matlines(d$time, t(env.mwe.eff$envelope), type="l", lty=3, lwd=1, col=rgb(0,0,1,1))
dev.off()

savename <- "intro_toyExample.png"
png(filename = file.path(savepath, savename),
    width = OPTS$plot$aspect.ratio*fig.height.inch , height = fig.height.inch, units="in", res=600)
matplot(d$time, t(d$sig), type="l", lty=1, col=rgb(0,0,0,0.01),
        xlab="Time (s)", ylab="Voltage (mV)")
matlines(d$time, t(env.qnt$envelope), type="l", lty=1, lwd=1, col=rgb(1,0,0,1))
matlines(d$time, t(env.mwe$envelope), type="l", lty=1, lwd=1, col=rgb(0,0,1,1))
matlines(d$time, t(env.mwe.eff$envelope), type="l", lty=3, lwd=1, col=rgb(0,0,1,1))
dev.off()
