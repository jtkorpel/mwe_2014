# A script to plot the NOAA GHCN temperature data

## Options
savepath <- file.path(OPTS$res.dir,'pubfigs/noaa_ghcn')
dir.create(savepath, recursive=T, showWarnings=F)

## Load fold profile
fold <- readRDS("./experiment_noaa_ghcn/ITE00100554_tmax_FWER_profile_MWE.rds")
(k.eff <- max(which(fold$profile <= alpha))-1) #3
opt.prof <- data.frame(k=1:length(fold$profile.k),
                       fwer=1:length(fold$profile.k)/length(fold$profile.k))

# Whole profile
fig.height.inch = OPTS$plot$fig.height.inch
aspect.ratio = 1
savename <- "ghcn_milan_monthly_profile_L245.pdf"
pdf(file = file.path(savepath, savename),
    width = aspect.ratio*fig.height.inch , height = fig.height.inch)

up.to.alpha = 0.1
up.to.k = 244
N = nrow(fold$profile.mat)
plot(fold$profile.k/N, fold$profile, ylim=c(0,1), type="l",
     xlab="K/N", ylab="FWER = % of obs. outside CB", xlim=c(0,1))
lines(opt.prof$k/N, opt.prof$fwer)
lines(matrix(c(0,alpha,1,alpha),nrow=2,byrow=T), lty=3, lwd=2)
dev.off()


# Zoom profile
fig.height.inch = OPTS$plot$fig.height.inch
aspect.ratio = 1
savename <- "ghcn_milan_monthly_profile_L245_zoom.pdf"
pdf(file = file.path(savepath, savename),
    width = aspect.ratio*fig.height.inch , height = fig.height.inch)

up.to.k = 20
plot(fold$profile.k[1:up.to.k]/N, fold$profile[1:up.to.k], type="p",
     xlab="K/N", ylab="FWER = % of obs. outside CB", ylim=c(0,0.2))
lines(opt.prof$k/N, opt.prof$fwer)
lines(matrix(c(k.eff/N,0,k.eff/N,0.4),nrow=2,byrow=T), lty=3, lwd=2)
lines(matrix(c(0,alpha,0.4,alpha),nrow=2,byrow=T), lty=3, lwd=2)
dev.off()

# For L=245 profile is [0.06938776 0.08979592 0.09795918 0.10204082 0.11020408 ...] -> for alpha=0.1 k.eff=3
# For L=50 profile is [0.07163265 0.11000000 0.11500000 ...] so for alpha=0.1 k.eff=1
# For L=10 profile is  [0.07284483 0.09784483 0.11034483 0.11867816...] so for alpha=0.1 k.eff=2
# The naive way is to set k=floor(nrow(data)*0.1)=24 

## Load data
data.list <- load.temperature.milan()
data <- data.list$sig

## Compute confidence intervals
ci.mwe <- mwe.ci(data, floor(alpha*nrow(data)))
env.mwe <- ci.envelope(data[!ci.mwe$is.extreme.obs,])

ci.mwe.eff <- mwe.ci(data, k.eff)
env.mwe.eff <- ci.envelope(data[!ci.mwe.eff$is.extreme.obs,])


## Plot data
fig.height.inch = OPTS$plot$fig.height.inch
aspect.ratio = OPTS$plot$aspect.ratio
savename <- "ghcn_milan_monthly_tmax.pdf"
pdf(file = file.path(savepath, savename),
    width = aspect.ratio*fig.height.inch , height = fig.height.inch)
tvec <- 1:12
matplot(tvec, t(data), type="l", lty=1, col=rgb(0,0,0,0.1),
        xlab="Month", ylab="Max temperature (C)", xaxt="n")
axis(1, at = 1:12, labels = c("jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec"), cex.axis = 1)
matlines(tvec, t(env.mwe$envelope), type="l", lty=1, lwd=1.5, col=rgb(0,0,0,1))
matlines(tvec, t(env.mwe.eff$envelope), type="l", lty=2, lwd=1.5, col=rgb(0,0,0,1))
matlines(tvec, t(data[ci.mwe.eff$extreme.obs.inds,]), type="b", lty=1, col=rgb(1,0,0,0.7))
dev.off()

# range(as.numeric(dimnames(data)[[1]]))
# dimnames(data)[[1]][ci.mwe.eff$extreme.obs.inds] #years 2003, 2006, 2007
