# A script to plot mitdb experiment for all methods
set.seed(42)

## Options

savepath <- file.path(OPTS$res.dir,'pubfigs/supplementary_material')
dir.create(savepath, recursive=T, showWarnings=F)


methods.list <- list(c("QUANTILE", "QUANTILE.C"),
                     c("DST.MAHA", "DST.MAHA.C"),
                     c("DST.L2", "DST.L2.C"),
                     c("MWE", "MWEC"),
                     c("KMEANS.AVEDST", "KMEANS.AVEDST.C"))
# Note: bonferroni and border control not applicable due to too small N (both need N > 2M/alpha).

## Helper functions

# Plot fold profile
make.profile.plot <- function(cb){
  N <- length(cb$is.extreme.obs)
  if("profile.k" %in% names(cb)){
    up.to.k = cb$k.eff + 2
    match = cb$profile.k <= up.to.k
    k.vec = cb$profile.k[match]
  } else {
    up.to.alpha = cb$alpha.eff + 2*cb$profile.alpha[2]
    up.to.k = up.to.alpha*N
    match = cb$profile.alpha <= up.to.alpha
    k.vec = cb$profile.alpha[match]*N
  }
  
  plot(k.vec, cb$profile.prc[match], type="o",
       xlab="k", ylab="% of obs. outside CI",
       main=sprintf("%s, k_eff: %1.0f, alpha.eff: %0.4f",cb$method, cb$k.eff, cb$alpha.eff))
  lines(matrix(c(0,alpha, up.to.k, alpha),nrow=2,byrow=T), lty=3, lwd=2)
}

# Plot data and confidence bands
make.data.plot <- function(data.n, cb1, cb2, rnd.inds){
  matplot(data.n$time, t(data.n$sig[rnd.inds,]), type="l", lty=1, col=rgb(0,0,0,0.1), ylim=c(-1.75,2.4),
          xlab="Time (s)", ylab="Voltage (mV)")
  legend("topright", c(cb1$method, cb2$method), lty=c(1,2), horiz=T)
  matlines(data.n$time, t(data.pvc$sig), type="l", lty=1, col=rgb(1,0,0,0.05))
  
  env1 <- ci.envelope(data.n$sig[!cb1$is.extreme.obs,])
  matlines(data.n$time, t(env1$envelope), type="l", lty=1, col=rgb(0,0,0,1))
  env2 <- ci.envelope(data.n$sig[!cb2$is.extreme.obs,])
  matlines(data.n$time, t(env2$envelope), type="l", lty=2, col=rgb(0,0,0,1))
}


## MITDB
savename = "mitdb_confbands_allMethods.pdf"

# Load data
srcpath <- file.path(OPTS$work.dir,'data_intermediate','batch.realdata')
srcfile <- file.path(srcpath, 'heartbeat.normal_nonsc_alpha0.10_ConfBandsNData.rds')
rl <- readRDS(srcfile)
data.n <- readRDS("./experiment_mitdb/mitdb106_beats_normal.rds")
data.pvc <- readRDS("./experiment_mitdb/mitdb106_beats_pvc.rds")
rnd.inds <- sample(1:nrow(data.n$sig), 100)

w = 8.5
h = 11.7
pdf(file.path(savepath,savename), width=w, height=h)

for (el in methods.list){
  cb1 <- rl$cb[[subset(rl$key, method==el[[1]])$pos]]
  cb2 <- rl$cb[[subset(rl$key, method==el[[2]])$pos]]
  layout(matrix(c(1,2), ncol=1))
  make.profile.plot(cb2)
  make.data.plot(data.n, cb1, cb2, rnd.inds)
}
dev.off()
