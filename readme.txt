This folder contains code related to the manuscript:

Confidence bands for time series data: Korpela Jussi, Puolamäki Kai and Aristides Gionis, Data Mining and Knowledge Discovery, DAMI-D-14-00080

For simplicity and completeness, the repository contains the datasets as well. Because of this, it is roughly 300 MB in size.

To reproduce all analyses start R with the directory of this readme.txt as the working directory and run:
source("reproduce.analyses.R")


A full run takes about ?? hours. It will create additional folders "data_intermediate", "pubfigs" and "pubtabs".

Included are:
reproduce.analyses.R    A script to reproduce every experiment, figure and table of the manuscript
init.R			            A script to initialize the analysis environment
experiment_*		        Source data and preprocessing for the real data experiments
precomp_CB		          Scripts to compute confidence bands and related measures for
			                  a variety of datasets and methods 
publication_plots	      Scripts to create the figures of the manuscript
publication_tables 	    Scripts to create the tables of the manuscript
supplementary_material  Supplementary result figures related to the manuscript
...


