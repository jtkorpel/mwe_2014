
# idstr = "common_extreme_observations"
# loadpath <- file.path(OPTS$work.dir,'data_intermediate/batch.synthdata')
# savepath.tab = file.path(OPTS$res.dir,'pubtabs')
# dir.create(savepath.tab, recursive=T, showWarnings=F)
# 
# 
# ## Load data
# data.set.id <- 'SHB_M25_synth.heartbeat.75prc_nonsc'
# data.file.name <- sprintf('%s_alpha%1.2f_ConfBandsNData.rds',data.set.id,alpha)
# rl <- readRDS(file.path(loadpath, data.file.name))
# data.loader.fun <- load.synth.heartbeat.75prc

savepath.tab = file.path(OPTS$res.dir,'pubtabs')
dir.create(savepath.tab, recursive=T, showWarnings=F)

## Load data
loadpath <- file.path(OPTS$work.dir,'data_intermediate/batch.synthdata')
nm.arr <- matrix(c(1e3, 25), ncol=2, byrow=T)
#nm.arr <- matrix(c(1e4, 25), ncol=2, byrow=T)
rl <- load.cbANDdata(loadpath, nm.arr,
                     "SHB", "synth.heartbeat.75prc_nonsc_alpha0.10_ConfBandsNData.rds")
data.set.id = unique(rl$key$data)


dkey <- subset(rl$key, method=="QUANTILE")[,c("N","M","R")]
dkey$pos <- 1:nrow(dkey)
nmr.levels <- unique(dkey[,c("N","M","R")])
# This works since all methods go through data sets in the same order in cb.looper()



## Compare number of common extreme observations
common.extr.obs <- function(key.sub, cb.list){
  method.names <- unique(key.sub$method)
  resmat <- matrix(1, nrow=length(method.names), ncol=length(method.names))
  
  for (i1 in 1:length(method.names)){
    for (i2 in 1:length(method.names)){ 
      ind1 <- subset(key.sub, method==method.names[i1])$pos[[1]]
      ind2 <- subset(key.sub, method==method.names[i2])$pos[[1]]
      
      resmat[i1,i2] <- length(intersect(cb.list[[ind1]]$extreme.obs.inds,
                                        cb.list[[ind2]]$extreme.obs.inds))
    }
  }
  dimnames(resmat) <- list(method.names, method.names)
  return(resmat) 
}

res.list <- list()
for(i in 1:nrow(nmr.levels)){
    key.sub <- subset(rl$key, N==nmr.levels[i,"N"] & M==nmr.levels[i,"M"] & R==nmr.levels[i,"R"])
    resmat <- common.extr.obs(key.sub, rl$cb)
    res.list <- c(res.list, list(resmat))    
} # N-M-R
nmr.levels$pos <- 1:nrow(nmr.levels)

## Export to latex format
n <-1000
m <- 25
r <- 2
tab <- res.list[[subset(nmr.levels, N==n & M==m & R==r)$pos]]
rownames(tab) <- dimnames(tab)[[1]]
print(tab, digits=2)


id.a <- "nonERC"
methods.a <- c("QUANTILE","BONFERRONI","DST.MAHA","DST.L2","MWE")
match.a <- dimnames(tab)[[1]] %in% methods.a
tab.a <- tab[match.a, match.a]
(tab.a)
rtb.latex.list <- create.latex.table(tab.a, num.format="%1.0f", col.just="c")
savename = sprintf('commonExtremeObsTable_%s_%s_N%d.txt', data.set.id, id.a, n)
write.latex.table(rtb.latex.list, file.path(savepath.tab, savename))


id.b <- "ERC"
methods.b <- c("QUANTILE.C","BONFERRONI.C","DST.MAHA.C","DST.L2.C","MWEC")
match.b <- dimnames(tab)[[1]] %in% methods.b
tab.b <- tab[match.b, match.b]
(tab.b)
rtb.latex.list <- create.latex.table(tab.b, num.format="%1.0f", col.just="c")
savename = sprintf('commonExtremeObsTable_%s_%s_N%d.txt', data.set.id, id.b, n)
write.latex.table(rtb.latex.list, file.path(savepath.tab, savename))



