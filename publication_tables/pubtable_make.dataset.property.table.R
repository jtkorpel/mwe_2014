# Creates dataset property table
# Note: Needs to be called from "reproduce.analyses.R" so that the environment is set up correctly

require(reshape)
require(plyr)


#######################################################################################
## Setup
loadpath.base <- file.path(OPTS$work.dir,'data_intermediate')
savepath.tab = file.path(OPTS$res.dir, 'pubtabs')
dir.create(savepath.tab, recursive=T, showWarnings=F)

n=1000
m=25
r=1

ds1 <- list(name="synth.heartbeat.0prc",
            file=file.path(loadpath.base, "batch.synthdata/SHB_N1000_M25_synth.heartbeat.0prc_nonsc_alpha0.10_ConfBandsNData.rds"))
ds2 <- list(name="synth.heartbeat.75prc",
            file=file.path(loadpath.base, "batch.synthdata/SHB_N1000_M25_synth.heartbeat.75prc_nonsc_alpha0.10_ConfBandsNData.rds"))
ds3 <- list(name="synth.heartbeat.100prc",
            file=file.path(loadpath.base, "batch.synthdata/SHB_N1000_M25_synth.heartbeat.100prc_nonsc_alpha0.10_ConfBandsNData.rds"))
ds4 <- list(name="heartbeat.normal",
            file=file.path(loadpath.base, "batch.realdata/heartbeat.normal_nonsc_alpha0.10_ConfBandsNData.rds"))
ds5 <- list(name="heartbeat.pvc",
            file=file.path(loadpath.base, "batch.realdata/heartbeat.pvc_nonsc_alpha0.10_ConfBandsNData.rds"))
ds6 <- list(name="temperature.milan",
            file=file.path(loadpath.base, "batch.realdata/temperature.milan_nonsc_alpha0.10_ConfBandsNData.rds"))
ds7 <- list(name="power.data",
            file=file.path(loadpath.base, "batch.realdata/power.data_nonsc_alpha0.10_ConfBandsNData.rds"))
dsl <- list(ds1,ds2,ds3,ds4,ds5,ds6,ds7)


#######################################################################################
## Helper functions

collect.ds.properties <- function(n,m,r,dsl){
  # Collect dataset properties in one data.frame
  dpdf <- data.frame()
  for (ds in dsl){
    
    cbl  <- readRDS(ds$file)
    dkey <- subset(cbl$key, method=="QUANTILE.C")[,c("N","M","R")]
    dkey$pos <- 1:nrow(dkey)
    nmr.levels <- unique(dkey[,c("N","M","R")])
    #browser()
    if (ds$name %in% c("synth.heartbeat.0prc","synth.heartbeat.75prc","synth.heartbeat.100prc")){
      dpos <- subset(dkey, N==n & M==m & R==r)$pos
      key <- subset(cbl$key, N==n & M==m & R==r & method %in% c("QUANTILE.C","BONFERRONI.C","DST.MAHA.C","DST.L2.C","MWEC"))
    } else {
      dpos <- 1
      key <- subset(cbl$key, method %in% c("QUANTILE.C","BONFERRONI.C","DST.MAHA.C","DST.L2.C","MWEC"))
    }
    env.data <- ci.envelope(cbl$data[[dpos]]$sig)
    
    for (ind in 1:nrow(key)){
      pos <- key[ind,"pos"]
      #   > names(cbl$cb[[5]])
      #   [1] "extreme.obs.inds" "is.extreme.obs"   "k"                "method"           "k.eff"           
      #   [6] "alpha.eff"        "L"                "k.profile"        "alpha"
      if (is.na(cbl$cb[[pos]]$alpha.eff)){
        env.prc <- NA
      } else {
        #browser()
        env.method <- ci.envelope(cbl$data[[dpos]]$sig[!cbl$cb[[pos]]$is.extreme.obs,])
        env.prc <- env.method$sum.width/env.data$sum.width
      }
      
      # method, N, M, alpha_envelope, alpha_eff, M_eff, CB width/envelope width
      dfline <- data.frame(dataset=ds$name,
                           method=cbl$cb[[pos]]$method,
                           N=key[ind,"N"],
                           M=key[ind,"M"],
                           R=key[ind,"R"],
                           alpha.env=cbl$cb[[pos]]$profile.prc[1],
                           alpha.eff=cbl$cb[[pos]]$alpha.eff,
                           k.eff=cbl$cb[[pos]]$k.eff,
                           env.prc=env.prc)
      dpdf <- rbind(dpdf, dfline)
    } #for nrow(key)
  } #for ds
  return(dpdf)
} #function

make.tab <- function(n,m,r,dsl){
  dpdf <- collect.ds.properties(n=n,m=m,r=r,dsl)
  tab <- subset(dpdf, method=="MWEC")[,-2]
  rownames(tab) <- tab$dataset
  tab <- tab[,!(names(tab) %in% c("dataset","R"))]
  tab <- as.matrix(tab)
  return(tab)
}

agg.fun <- function(df){
  # Compute aggregate measures, to be used with plyr::ddply
  ret.df <- data.frame( dlen=length(df$value),
                        mean=mean(df$value, na.rm=T),
                        std=stderr(df$value))
  return(ret.df)
}


#######################################################################################
## Collect and format the base table

dpdf <- collect.ds.properties(n=1000,m=25,r=4,dsl)
tab <- subset(dpdf, method=="MWEC")[,-2]
rownames(tab) <- tab$dataset
tab <- tab[,!(names(tab) %in% c("dataset","R"))]
tab <- as.matrix(tab)
#print(tab, digits=2)

# Export to latex
rtb.latex.list <- create.latex.table(tab, num.format="%1.3f", col.just="c")
savename = "dataset_property_table_N1000_M25_R4.txt"
write.latex.table(rtb.latex.list, file.path(savepath.tab, savename))


#######################################################################################
## Compute dataset property mean and se over iterations R for the synthetic datasets
# Note: this is not applicable to real datasets
r.arr <- 1:5
n=1000
m=25
tb <- data.frame()
for (r in r.arr){
  tab.tmp <- collect.ds.properties(n=n,m=m,r=r,dsl)
  tab.tmp <- subset(tab.tmp, dataset %in% c("synth.heartbeat.0prc","synth.heartbeat.75prc","synth.heartbeat.100prc"))
  tb <- rbind(tb, tab.tmp)
}

tbm <- reshape::melt(tb, id.vars=c("dataset","method","N","M","R"))

tbma <- plyr::ddply(tbm, .(dataset, method, N, M, variable), agg.fun)
tbma$feature <- tbma$variable
tbma$variable <- NULL
tbma <- reshape::melt(tbma, id.vars=c("dataset","method","N","M","feature"))
tbma <- subset(tbma, method=="MWEC")[,-2]
tbma <- subset(tbma, feature %in% c("alpha.env","alpha.eff","k.eff","env.prc") & variable %in% c("mean","std"))
tbma$featvar <- paste(tbma$feature, tbma$variable, sep="_")
tbma$feature <- NULL
tbma$variable <- NULL
tbma <- cast(tbma, dataset~featvar)
tbmam <- as.matrix(as.data.frame(tbma[,-1]))
row.names(tbmam) <- tbma$dataset
# print(tbmam[,c("alpha.env_mean","alpha.eff_mean","k.eff_mean","env.prc_mean")], digits=3)
# print(tbmam[,c("alpha.env_std","alpha.eff_std","k.eff_std","env.prc_std")], digits=3)
tb.mean <- tbmam[,c("alpha.env_mean","alpha.eff_mean","k.eff_mean","env.prc_mean")]
tb.sd <- tbmam[,c("alpha.env_std","alpha.eff_std","k.eff_std","env.prc_std")]

rtb.latex.list <- create.latex.table.pair(tb.mean, tb.sd, num.format="%1.3f", col.just="c")
savename = "dataset_property_table_N1000_M25_synthdata.txt"  
write.latex.table(rtb.latex.list, file.path(savepath.tab, savename))
