#!/bin/bash

rsync -avzr --delete --exclude-from '/home/jussi/work/svn/confidence_region/rsync_exclude_code.txt' /home/jussi/work/svn/confidence_region/code/R/ /home/jussi/work/bitbucket/mwe_2014/

#mkdir /home/jussi/work/bitbucket/MWE_2014/supplementary_material 

rsync -avzr --delete /home/jussi/work/svn/confidence_region/code/R/pubfigs/supplementary_material /home/jussi/work/bitbucket/mwe_2014/ 
